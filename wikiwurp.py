import requests 
from bs4 import BeautifulSoup

class WikiWurper():
    def __init__(self, url):
        self.url = url
    
    def get_original_page(self):
        return requests.get(self.url).content
                
    def wurp_it(self, html_content):
        return ContentWurper(self.url).feed(html_content)
        
class ContentWurper():
    def __init__(self, url):
        self.output = ''
        self.url = url
        
    def feed(self, original_html):
        parsed_html = BeautifulSoup(original_html, 'html.parser')
        for element in parsed_html.find_all(text=True):
            element.replace_with(self.wurp_when_five_chars_long(element.string))
            
        return str(parsed_html)
        
    def wurp_when_five_chars_long(self, original_text):
        if not original_text:
            return ' '
            
        wurped_text = []
        for word in original_text.split():
            ## according to some articles .replace().replace() is faster then re.compile() then .sub() hence usign this here:
            if len(word.replace('[', '').replace('(', '').replace(']', '').replace(')', '')) != 5:
                wurped_word = word
                
            if len(word.replace('[', '').replace('(', '').replace(']', '').replace(')', '')) == 5:
                wurped_word = ''
                ## some more time could maybe be spent with this bit (handling words in () and []) - above we obviously checked the word length without the parentheses and brackets
                if '[' in word:
                    wurped_word += '[' 
                if '(' in word:
                    wurped_word += '(' 
                    
                wurped_word += 'WURP!'
                
                if ']' in word:
                    wurped_word += ']' 
                if ')' in word:
                    wurped_word += ')' 
                    
            wurped_text.append(wurped_word)
            
        return str(' '.join(wurped_text))+' '
        