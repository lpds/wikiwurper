from flask import Flask, Response, request
from flask_restful import Resource, Api
import requests

app = Flask(__name__)
webserver = Api(app)

class WurpProxy(Resource):
	def get(self, page):
		from wikiwurp import WikiWurper
		wurper = WikiWurper('https://en.wikipedia.org/wiki/{page}'.format(page=page))
		html_content = wurper.get_original_page()
		return Response(wurper.wurp_it(html_content), 200)
webserver.add_resource(WurpProxy, '/wiki/<page>', methods=['GET'], endpoint='WurpProxy')

@app.route('/<path:path>')
def catch_all(path):
    proxied_path = 'https://en.wikipedia.org/'+path
    if len(request.args) > 0:
        proxied_path += '?'
        for get_param in request.args:
            proxied_path += get_param+'='+request.args[get_param]+'&'
        proxied_path=proxied_path[:-1]
    return Response(requests.get(proxied_path).content, 200)

if __name__ == '__main__':
	app.run(debug=True, host='0.0.0.0', port=int("8080"))
